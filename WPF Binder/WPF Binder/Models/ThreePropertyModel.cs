﻿namespace WPF_Binder.Models
{
    public class ThreePropertyModel
    {
        public ThreePropertyModel(string defaultString)
        {
            Prop1 = defaultString;
            Prop2 = defaultString;
            Prop3 = defaultString;
        }

        public string Prop1 {
            get; set;
        }

        public string Prop2 {
            get; set;
        }

        public string Prop3 {
            get; set;
        }
    }
}
