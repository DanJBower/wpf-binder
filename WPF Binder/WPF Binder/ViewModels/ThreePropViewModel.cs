﻿using System.Diagnostics;
using System.Timers;
using WPF_Binder.Data;
using WPF_Binder.Models;
using WPF_Binder.Utility;

namespace WPF_Binder.ViewModels
{
    public class ThreePropViewModel : NotifyPropertyChange
    {
        private ThreePropertyModel threeProp;
        
        //Method 2
        public string Prop1 {
            get => threeProp.Prop1;
            set => TrySetProperty(x => threeProp.Prop1 = x, value, threeProp.Prop2);
        }

        //Method 3
        public string Prop2 {
            get => threeProp.Prop2;
            set => TrySetProperty(threeProp, value, x => x.Prop2);
        }

        //Method 4
        public string Prop3 {
            get => threeProp.Prop3;
            set => TrySetProperty(threeProp, value, nameof(threeProp.Prop3));
        }

        public ThreePropViewModel()
        {
            if (Constants.MODE == Mode.EXAMPLE)
            {
                threeProp = new ThreePropertyModel(Constants.EXAMPLE_DEFAULT_STRING_VAL);
                Timer updateProp1Timer = new Timer();
                updateProp1Timer.Elapsed += new ElapsedEventHandler(OnTimedEventProp2);
                updateProp1Timer.Interval = 145;
                updateProp1Timer.Enabled = true;

                Timer updateProp2Timer = new Timer();
                updateProp2Timer.Elapsed += new ElapsedEventHandler(OnTimedEventProp3);
                updateProp2Timer.Interval = 50;
                updateProp2Timer.Enabled = true;

                Timer updateProp3Timer = new Timer();
                updateProp3Timer.Elapsed += new ElapsedEventHandler(OnTimedEventProp4);
                updateProp3Timer.Interval = 452;
                updateProp3Timer.Enabled = true;
            }
            else if (Constants.MODE == Mode.DUMMY_PERFORMANCE)
            {
                threeProp = new ThreePropertyModel(Constants.PERFORMANCE_DEFAULT_STRING_VAL);
            }
            else if (Constants.MODE == Mode.PERFORMANCE)
            {
                threeProp = new ThreePropertyModel(Constants.PERFORMANCE_DEFAULT_STRING_VAL);
            }
        }

        public void UpdateDelegateProperty(Stopwatch sw, string newVal)
        {
            sw.Start();
            Prop1 = newVal;
            sw.Stop();
        }

        public void UpdateLINQProperty(Stopwatch sw, string newVal)
        {
            sw.Start();
            Prop2 = newVal;
            sw.Stop();
        }

        public void UpdateReflectionProperty(Stopwatch sw, string newVal)
        {
            sw.Start();
            Prop3 = newVal;
            sw.Stop();
        }

        private void OnTimedEventProp2(object source, ElapsedEventArgs e)
        {
            Prop1 = string.Format(Constants.EXAMPLE_NUMBER_FORMAT, AccessData.GetDataVal(51, 561));
        }

        private void OnTimedEventProp3(object source, ElapsedEventArgs e)
        {
            Prop2 = string.Format(Constants.EXAMPLE_NUMBER_FORMAT, AccessData.GetDataVal(1, 84945));
        }

        private void OnTimedEventProp4(object source, ElapsedEventArgs e)
        {
            Prop3 = string.Format(Constants.EXAMPLE_NUMBER_FORMAT, AccessData.GetDataVal(0, 895));
        }

        public long RunTestMethod2()
        {
            Stopwatch s = new Stopwatch();

            for (int i = 0; i < Constants.PERFORMANCE_LOOP; i++)
            {
                string newVal = string.Format(Constants.PERFORMANCE_NUMBER_FORMAT, AccessData.GetDataVal(Constants.MIN_RAND, Constants.MAX_RAND));

                s.Start();
                Method2(x => threeProp.Prop1 = x, newVal);
                s.Stop();
            }
            return s.ElapsedMilliseconds;
        }

        public long RunTestMethod3()
        {
            Stopwatch s = new Stopwatch();

            for (int i = 0; i < Constants.PERFORMANCE_LOOP; i++)
            {
                string newVal = string.Format(Constants.PERFORMANCE_NUMBER_FORMAT, AccessData.GetDataVal(Constants.MIN_RAND, Constants.MAX_RAND));

                s.Start();
                Method3(threeProp, newVal, x => x.Prop2);
                s.Stop();
            }
            return s.ElapsedMilliseconds;
        }

        public long RunTestMethod4()
        {
            Stopwatch s = new Stopwatch();

            for (int i = 0; i < Constants.PERFORMANCE_LOOP; i++)
            {
                string newVal = string.Format(Constants.PERFORMANCE_NUMBER_FORMAT, AccessData.GetDataVal(Constants.MIN_RAND, Constants.MAX_RAND));

                s.Start();
                Method4(threeProp, newVal, nameof(threeProp.Prop3));
                s.Stop();
            }
            return s.ElapsedMilliseconds;
        }
    }
}
