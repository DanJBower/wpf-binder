﻿using System;

namespace WPF_Binder.Data
{
    public static class AccessData
    {
        private static Random random = new Random();
        public static int GetDataVal()
        {
            return GetDataVal(1, 10);
        }

        public static int GetDataVal(int min, int max)
        {
            return random.Next(min, max + 1);
        }
    }
}
