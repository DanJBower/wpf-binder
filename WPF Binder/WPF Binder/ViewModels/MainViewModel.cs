﻿using System.Diagnostics;
using System.Threading.Tasks;
using System.Timers;
using WPF_Binder.Data;
using WPF_Binder.Utility;

namespace WPF_Binder.ViewModels
{
    public class MainViewModel : NotifyPropertyChange
    {
        private string performanceTestField = string.Empty;
        public string PerformanceTestField {
            get => performanceTestField;
            set => TrySetField(ref performanceTestField, value);
        }

        private string performanceDelegateTest = string.Empty;
        public string PerformanceDelegateTest {
            get => performanceDelegateTest;
            set => TrySetField(ref performanceDelegateTest, value);
        }

        private string performanceLINQTest = string.Empty;
        public string PerformanceLINQTest {
            get => performanceLINQTest;
            set => TrySetField(ref performanceLINQTest, value);
        }

        private string performanceReflectionTest = string.Empty;
        public string PerformanceReflectionTest {
            get => performanceReflectionTest;
            set => TrySetField(ref performanceReflectionTest, value);
        }

        private string fieldProp;

        //Method 1
        public string FieldProp {
            get => fieldProp;
            set => TrySetField(ref fieldProp, value);
        }

        private ThreePropViewModel threeProp = new ThreePropViewModel();
        public ThreePropViewModel ThreeProp {
            get => threeProp;
            set => TrySetField(ref threeProp, value);
        }

        public MainViewModel()
        {
            if (Constants.MODE == Mode.EXAMPLE)
            {
                FieldProp = Constants.EXAMPLE_DEFAULT_STRING_VAL;
                Timer updateFieldPropTimer = new Timer();
                updateFieldPropTimer.Elapsed += new ElapsedEventHandler(ExampleFieldPropertyTest);
                updateFieldPropTimer.Interval = 1000;
                updateFieldPropTimer.Enabled = true;
            }
            else if(Constants.MODE == Mode.DUMMY_PERFORMANCE)
            {
                FieldProp = Constants.PERFORMANCE_DEFAULT_STRING_VAL;
                DummyPerformanceTest();
            }
            else if (Constants.MODE == Mode.PERFORMANCE)
            {
                FieldProp = Constants.PERFORMANCE_DEFAULT_STRING_VAL;
                PerformanceTest();
            }
        }

        private void ExampleFieldPropertyTest(object source, ElapsedEventArgs e)
        {
            FieldProp = string.Format(Constants.EXAMPLE_NUMBER_FORMAT, AccessData.GetDataVal());
        }

        private string dummyPerformancePrefix = $"{Constants.PERFORMANCE_PREFIX}{Constants.DUMMY_PERFORMANCE_LOOP} runs: ";
        private string performancePrefix = $"{Constants.PERFORMANCE_PREFIX}{Constants.PERFORMANCE_LOOP} runs: ";

        private void PerformanceTest()
        {
            PerformanceTestField = $"{performancePrefix}{RunTestMethod1()}ms";
            PerformanceDelegateTest = $"{performancePrefix}{threeProp.RunTestMethod2()}ms";
            PerformanceLINQTest = $"{performancePrefix}{threeProp.RunTestMethod3()}ms";
            PerformanceReflectionTest = $"{performancePrefix}{threeProp.RunTestMethod4()}ms";
        }

        private long RunTestMethod1()
        {
            Stopwatch s = new Stopwatch();

            for (int i = 0; i < Constants.PERFORMANCE_LOOP; i++)
            {
                string newVal = string.Format(Constants.PERFORMANCE_NUMBER_FORMAT, AccessData.GetDataVal(Constants.MIN_RAND, Constants.MAX_RAND));

                s.Start();
                Method1(ref performanceTestField, newVal);
                s.Stop();
            }
            return s.ElapsedMilliseconds;
        }

        private async void DummyPerformanceTest()
        {
            await Task.Delay(Constants.DUMMY_PERFORMANCE_MAJOR_DELAY);
            Stopwatch s1 = new Stopwatch();

            for (int i = 0; i < Constants.DUMMY_PERFORMANCE_LOOP; i++)
            {
                string newVal = string.Format(Constants.PERFORMANCE_NUMBER_FORMAT, AccessData.GetDataVal(Constants.MIN_RAND, Constants.MAX_RAND));
                
                s1.Start();
                FieldProp = newVal;
                s1.Stop();

                PerformanceTestField = $"                Performance Test {i + 1} out of {Constants.DUMMY_PERFORMANCE_LOOP}";

                await Task.Delay(Constants.DUMMY_PERFORMANCE_MINOR_DELAY);
            }

            PerformanceTestField = $"{dummyPerformancePrefix}{s1.ElapsedMilliseconds}ms";

            await Task.Delay(Constants.DUMMY_PERFORMANCE_MAJOR_DELAY);
            Stopwatch s2 = new Stopwatch();

            for (int i = 0; i < Constants.DUMMY_PERFORMANCE_LOOP; i++)
            {
                string newVal = string.Format(Constants.PERFORMANCE_NUMBER_FORMAT, AccessData.GetDataVal(Constants.MIN_RAND, Constants.MAX_RAND));

                threeProp.UpdateDelegateProperty(s2, newVal);

                PerformanceDelegateTest = $"                Performance Test {i + 1} out of {Constants.DUMMY_PERFORMANCE_LOOP}";

                await Task.Delay(Constants.DUMMY_PERFORMANCE_MINOR_DELAY);
            }

            PerformanceDelegateTest = $"{dummyPerformancePrefix}{s2.ElapsedMilliseconds}ms";

            await Task.Delay(Constants.DUMMY_PERFORMANCE_MAJOR_DELAY);
            Stopwatch s3 = new Stopwatch();

            for (int i = 0; i < Constants.DUMMY_PERFORMANCE_LOOP; i++)
            {
                string newVal = string.Format(Constants.PERFORMANCE_NUMBER_FORMAT, AccessData.GetDataVal(Constants.MIN_RAND, Constants.MAX_RAND));

                threeProp.UpdateLINQProperty(s3, newVal);

                PerformanceLINQTest = $"                Performance Test {i + 1} out of {Constants.DUMMY_PERFORMANCE_LOOP}";

                await Task.Delay(Constants.DUMMY_PERFORMANCE_MINOR_DELAY);
            }

            PerformanceLINQTest = $"{dummyPerformancePrefix}{s3.ElapsedMilliseconds}ms";
            
            await Task.Delay(Constants.DUMMY_PERFORMANCE_MAJOR_DELAY);
            Stopwatch s4 = new Stopwatch();

            for (int i = 0; i < Constants.DUMMY_PERFORMANCE_LOOP; i++)
            {
                string newVal = string.Format(Constants.PERFORMANCE_NUMBER_FORMAT, AccessData.GetDataVal(Constants.MIN_RAND, Constants.MAX_RAND));

                threeProp.UpdateReflectionProperty(s4, newVal);

                PerformanceReflectionTest = $"                Performance Test {i + 1} out of {Constants.DUMMY_PERFORMANCE_LOOP}";

                await Task.Delay(Constants.DUMMY_PERFORMANCE_MINOR_DELAY);
            }

            PerformanceReflectionTest = $"{dummyPerformancePrefix}{s4.ElapsedMilliseconds}ms";
        }
    }
}
