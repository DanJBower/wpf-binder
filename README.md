# WPF Binder #

This repository contains a utility class called `NotifyPropertyChange`. The best way to get the file, `NotifyPropertyChange.cs` is from the top level of this repository or press this [link to go to it](NotifyPropertyChange.cs)

It is designed to make it easier to raise a property change in your view models. In order to use this class, make it the base class of your view model (or implement it into your own base view model class).

    public class YourViewModel : NotifyPropertyChange

## Methods ##

There are four methods that are accessible in a derived class;

 * Method 1 - [TrySetField<T>(ref T field, T newValue, bool performEqualityCheck, string propertyName)](#markdown-header-method-1-trysetfield)
 * Method 2 - [TrySetProperty<T>(Action<T> property, T newValue, T oldValue, bool performEqualityCheck, string propertyName)](#markdown-header-method-2-trysetproperty)
 * Method 3 - [TrySetProperty<T1, T2>(T1 model, T2 newValue, Expression<Func<T1, T2>> propertyExpression, bool performEqualityCheck, string propertyName)](#markdown-header-method-3-trysetproperty)
 * Method 4 - [TrySetProperty<T1, T2>(T2 model, T1 newValue, string targetPropertyName, bool performEqualityCheck, string propertyName)](#markdown-header-method-4-trysetproperty)

### Method 1 - TrySetField ###

Method 1 is for updating a **Field**. This is the fastest method as all it does is assign a value to the accessible field by passing the field by reference into the method.

#### Parameters ####
| Parameter | Description | Default Value | Example |
| :---: | :---: | :---: | :---- |
| ref T field | A reference to the field that stores the value for the property being updated |  | `ref name` |
| T newValue | The property's new value |  | `"Hello"` |
| bool performEqualityCheck | Determines whether an equality check will be run or if it will be skipped | `true` | `false` |
| string propertyName | The name of the property being updated. If left null, this value will be set to the CallerMemberName | `null` | `null` |

#### Returns ####
This method returns a boolean.

When the boolean is `true`, it means program was able to successfully set the variable and raise a property change to notify the UI.

When it is `false`, it means the program did not change the value of the property either because the values were equal and the equality check was enabled or because the property does not exist.

If the values are equal and `performEqualityCheck` is `true`, it returns false because we do not want to use extra processing time raising the property change to the UI if it is exactly the same.

If the property does not exist then it returns false because there is nothing to update.

#### Example ####

	private string field;
	public string Field
	{
		get => field;
		set => TrySetField(ref field, value);
	}

### Method 2 - TrySetProperty ###

Method 2 is for updating a **Property**. This is the fastest method for updating a property. It uses an **Action** to set the property in an other class.

#### Parameters ####
| Parameter | Description | Default Value | Example |
| :---: | :---: | :---: | :---- |
| Action<T> property | An action used for setting the property |  | `x => model.Prop = x` |
| T newValue | The property's new value |  | `"Hello"` |
| T oldValue | The property's old value for equality check |  | `model.Prop` |
| bool performEqualityCheck | Determines whether an equality check will be run or if it will be skipped | `true` | `false` |
| string propertyName | The name of the property being updated. If left null, this value will be set to the CallerMemberName | `null` | `null` |

#### Returns ####
This method returns a boolean.

When the boolean is `true`, it means program was able to successfully set the variable and raise a property change to notify the UI.

When it is `false`, it means the program did not change the value of the property either because the values were equal and the equality check was enabled or because the property does not exist.

If the values are equal and `performEqualityCheck` is `true`, it returns false because we do not want to use extra processing time raising the property change to the UI if it is exactly the same.

If the property does not exist then it returns false because there is nothing to update.

#### Example ####

	public int Prop {
		get => model.Prop;
		set => TrySetProperty(x => model.Prop = x, value, model.Prop);
	}

### Method 3 - TrySetProperty ###

Method 3 is also for updating a **Property**. It uses a **LINQ expression** to set the property in an other class.

#### Parameters ####
| Parameter | Description | Default Value | Example |
| :---: | :---: | :---: | :---- |
| T1 model | The variable that stores the property that stores the value being updated |  | `model` |
| T2 newValue | The property's new value |  | `"Hello"` |
| Expression<Func<T1, T2>> propertyExpression | The LINQ expression that determines the property to be updated |  | `x => x.Prop` |
| bool performEqualityCheck | Determines whether an equality check will be run or if it will be skipped | `true` | `false` |
| string propertyName | The name of the property being updated. If left null, this value will be set to the CallerMemberName | `null` | `null` |

#### Returns ####
This method returns a boolean.

When the boolean is `true`, it means program was able to successfully set the variable and raise a property change to notify the UI.

When it is `false`, it means the program did not change the value of the property either because the values were equal and the equality check was enabled or because the property does not exist.

If the values are equal and `performEqualityCheck` is `true`, it returns false because we do not want to use extra processing time raising the property change to the UI if it is exactly the same.

If the property does not exist then it returns false because there is nothing to update.

#### Example ####

	public int Prop {
		get => model.Prop;
		set => TrySetProperty(model, value, x => x.Prop);
	}

### Method 4 - TrySetProperty ###

Method 4 is also for updating a **Property**. It uses a **Reflection** to set the property in an other class.

#### Parameters ####
| Parameter | Description | Default Value | Example |
| :---: | :---: | :---: | :---- |
| T2 model | The variable that stores the property that stores the value being updated |  | `model` |
| T1 newValue | The property's new value |  | `"Hello"` |
| string targetPropertyName | The name of the property that will be updated in the other class |  | `nameof(model.Prop)` |
| bool performEqualityCheck | Determines whether an equality check will be run or if it will be skipped | `true` | `false` |
| string propertyName | The name of the property being updated. If left null, this value will be set to the CallerMemberName | `null` | `null` |

#### Returns ####
This method returns a boolean.

When the boolean is `true`, it means program was able to successfully set the variable and raise a property change to notify the UI.

When it is `false`, it means the program did not change the value of the property either because the values were equal and the equality check was enabled or because the property does not exist.

If the values are equal and `performEqualityCheck` is `true`, it returns false because we do not want to use extra processing time raising the property change to the UI if it is exactly the same.

If the property does not exist then it returns false because there is nothing to update.

#### Example ####

	public int Prop {
		get => model.Prop;
		set => TrySetProperty(model, value, nameof(model.Prop));
	}

## Performance ##

| Method | Core Code (Code run in Test) | Time to run 10000000 (10 million) times | Average Run Time |
| :---: | :---- | :---: | :---: |
| 1 | `field = newValue;` | 673ms | 67.3ns |
| 2 | `property(newValue);` | 862ms | 86.2ns |
| 3 | `MemberExpression expr = (MemberExpression)propertyExpression.Body;          PropertyInfo prop = (PropertyInfo)expr.Member;          prop.SetValue(model, newValue, null);` | 49183ms | 4918.3ns |
| 4 | `PropertyInfo prop = model.GetType().GetProperty(targetPropertyName);          prop.SetValue(model, newValue, null);` | 6073ms | 607.3ns |

## Modes ##

There are three modes defined in `WPF_Binder.Data.Mode`

| Mode | Description |
| :---: | :---- |
| EXAMPLE | EXAMPLE just shows all 4 methods working |
| DUMMY_PERFORMANCE | DUMMY_PERFORMANCE shows all 4 methods working individually and compares how long each one takes. This is not very accurate and more just shows a little example how the actual performance test works |
| PERFORMANCE | PERFORMANCE doesn't show any of the methods working. Instead it runs a stripped back version of each method 10000000 (10 million) times. The stripped back version is the core code that makes each method individual. This means there is no equality checks or UI update calls as these are common to each method. Similarly, there are no return values as those are common to each method as well. This happens before the UI is loaded so when the program is started, it takes approximately 60 seconds for the UI to appear. |
