﻿namespace WPF_Binder.Data
{
    public static class Constants
    {
        public const Mode MODE = Mode.EXAMPLE;

        public const string EXAMPLE_NUMBER_FORMAT = "{0}";
        public const string EXAMPLE_DEFAULT_STRING_VAL = "0";
        public const string PERFORMANCE_NUMBER_FORMAT = "{0:D5}";
        public const string PERFORMANCE_DEFAULT_STRING_VAL = "00000";
        public const string PERFORMANCE_PREFIX = "                Performance for ";

        public const int DUMMY_PERFORMANCE_LOOP = 50;
        public const int DUMMY_PERFORMANCE_MAJOR_DELAY = 2000;
        public const int DUMMY_PERFORMANCE_MINOR_DELAY = 200;
        public const int PERFORMANCE_LOOP = 10000000;
        public const int MIN_RAND = 0;
        public const int MAX_RAND = 99999;
    }
}
