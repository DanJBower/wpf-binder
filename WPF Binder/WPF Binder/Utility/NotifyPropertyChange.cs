﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace WPF_Binder.Utility
{
    /// <summary>
    /// Ideas for this class from <para/>
    /// Implementing INotifyPropertyChanged - does a better way exist? - https://stackoverflow.com/a/1316417/4601149 <para/>
    /// Passing properties by reference in C# - https://stackoverflow.com/a/1403036/4601149
    /// </summary>
    public abstract class NotifyPropertyChange : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// This method lets the system know that a properties value has been changed.
        /// </summary>
        /// <param name="sender">Origin of call</param>
        /// <param name="propertyName">The name of the property being updated <para/>
        /// If not specified, the CallerMemberName will be used</param>
        /// <returns>Returns a boolean. True if successfully raised a property change. Returns false if it is not a valid property name or if PropertyChanged / the sender object is null</returns>
        private bool TryRaisePropertyChanged(object sender, [CallerMemberName] string propertyName = null)
        {
			if(PropertyChanged == null || sender == null || !IsValidPropertyName(sender, propertyName))
			{
				return false;
			}

            PropertyChanged.Invoke(sender, new PropertyChangedEventArgs(propertyName));
			return true;
        }

        /// <summary>
        /// This method validates the property name is valid before trying to raise the property change
        /// </summary>
        /// <param name="sender">Origin of call</param>
        /// <param name="propertyName">The property name to check</param>
        /// <returns>Returns a boolean. True if it is a valid property name. False if it is not a valid property name</returns>
        private static bool IsValidPropertyName(object sender, string propertyName)
		{
			if (TypeDescriptor.GetProperties(sender)[propertyName] == null)
			{
				return false;
			}
			return true;
		}

        /// <summary>
        /// This method compares two values to see if they are equal.
        /// </summary>
        /// <typeparam name="T">The type of variable being compared</typeparam>
        /// <param name="x">X is compared to Y</param>
        /// <param name="y">Y is compared to X</param>
        /// <returns>Returns a boolean. <para/>
        /// If it is true, the two values are equal. <para/>
        /// If it is false the two values are not equal</returns>
        /// <example>
        /// <code>
        /// int x = 1;
        /// int y = 2;
        /// Compare(x, y);
        /// </code>
        /// </example>
        private static bool IsEqual<T>(T x, T y)
        {
            return EqualityComparer<T>.Default.Equals(x, y);
        }

        #region Update Fields
        //Method 1
        /// <summary>
        /// Updates a property that uses an accessible field to store the property's value.
        /// </summary>
        /// <typeparam name="T">The type of property being updated</typeparam>
        /// <param name="field">Reference to the field that stores the property's value</param>
        /// <param name="newValue">The property's new value</param>
        /// <param name="performEqualityCheck">Defines whether the equality check is done</param>
        /// <param name="propertyName">The property being updated. This is the CallerMemberName if it is not explicitly defined</param>
        /// <returns>A boolean. If the values are equal, the boolean is false as the property has not changed. Otherwise it returns true</returns>
        /// <example>
        /// <code>
        /// private string field;
        /// public string Field
        /// {
        ///     get => field;
        ///     set => TrySetField(ref field, value);
        /// }
        /// </code>
        /// </example>
        protected bool TrySetField<T>(ref T field, T newValue, bool performEqualityCheck = true, [CallerMemberName] string propertyName = null)
        {
            if(performEqualityCheck && IsEqual(field, newValue))
            {
                return false;
            }

            field = newValue;

            if(!TryRaisePropertyChanged(this, propertyName))
            {
                return false;
            }

            return true;
        }
        #endregion

        #region Update Properties
        //Method 2 - Action - Fastest TrySetProperty method
        /// <summary>
        /// Updates a property that uses an accessible property to store the property's value.
        /// </summary>
        /// <typeparam name="T">The type of property being updated</typeparam>
        /// <param name="property">An action used for setting the property. See example of how to write</param>
        /// <param name="newValue">The property's new value</param>
        /// <param name="oldValue">The property's old value for equality check</param>
        /// <param name="performEqualityCheck">Defines whether the equality check is done</param>
        /// <param name="propertyName">The property being updated. This is the CallerMemberName if it is not explicitly defined</param>
        /// <returns>A boolean. If the values are equal, the boolean is false as the property has not changed. Otherwise it returns true</returns>
        /// <example>
        /// <code>
        /// public int Prop {
        /// 	get => model.Prop;
        /// 	set => TrySetProperty(x => model.Prop = x, value, model.Prop);
        /// }
        /// </code>
        /// </example>
        protected bool TrySetProperty<T>(Action<T> property, T newValue, T oldValue, bool performEqualityCheck = true, [CallerMemberName] string propertyName = null)
        {
            if (performEqualityCheck && IsEqual(oldValue, newValue))
            {
                return false;
            }

            property(newValue);

            if (!TryRaisePropertyChanged(this, propertyName))
            {
                return false;
            }

            return true;
        }

        //Method 3 - LINQ Expression
        /// <summary>
        /// Updates a property that uses an accessible property to store the property's value.
        /// </summary>
        /// <typeparam name="T1">The type of variable that holds the property</typeparam>
        /// <typeparam name="T2">The type of property being updated</typeparam>
        /// <param name="model">The variable that stores the property that stores the value being updated</param>
        /// <param name="newValue">The property's new value</param>
        /// <param name="propertyExpression">The LINQ expression that determines the property to be updated. See example of how to write</param>
        /// <param name="performEqualityCheck">Defines whether the equality check is done</param>
        /// <param name="propertyName">The property being updated. This is the CallerMemberName if it is not explicitly defined</param>
        /// <returns>A boolean. If the values are equal, the boolean is false as the property has not changed. Otherwise it returns true</returns>
        /// <example>
        /// <code>
        /// public int Prop {
        /// 	get => model.Prop;
        /// 	set => TrySetProperty(model, value, x => x.Prop);
        /// }
        /// </code>
        /// </example>
        protected bool TrySetProperty<T1, T2>(T1 model, T2 newValue, Expression<Func<T1, T2>> propertyExpression, bool performEqualityCheck = true, [CallerMemberName] string propertyName = null)
        {
            MemberExpression expr = (MemberExpression)propertyExpression.Body;
            PropertyInfo prop = (PropertyInfo)expr.Member;
            
            if (performEqualityCheck && IsEqual(prop.GetValue(model), newValue))
            {
                return false;
            }

            prop.SetValue(model, newValue, null);

            if (!TryRaisePropertyChanged(this, propertyName))
            {
                return false;
            }

            return true;
        }

        //Method 4 - Reflection
        /// <summary>
        /// Updates a property that uses an accessible property to store the property's value.
        /// </summary>
        /// <typeparam name="T1">The type of variable that holds the property</typeparam>
        /// <typeparam name="T2">The type of property being updated</typeparam>
        /// <param name="model">The variable that stores the property that stores the value being updated</param>
        /// <param name="newValue">The property's new value</param>
        /// <param name="targetPropertyName">The name of the property that will be updated in the other class</param>
        /// <param name="performEqualityCheck">Defines whether the equality check is done</param>
        /// <param name="propertyName">The property being updated. This is the CallerMemberName if it is not explicitly defined</param>
        /// <returns>A boolean. If the values are equal, the boolean is false as the property has not changed. Otherwise it returns true</returns>
        /// <example>
        /// <code>
        /// public int Prop {
        /// 	get => model.Prop;
        /// 	set => TrySetProperty(model, value, nameof(model.Prop));
        /// }
        /// </code>
        /// </example>
        protected bool TrySetProperty<T1, T2>(T2 model, T1 newValue, string targetPropertyName, bool performEqualityCheck = true, [CallerMemberName] string propertyName = null)
        {
            PropertyInfo prop = model.GetType().GetProperty(targetPropertyName);

            if (performEqualityCheck && IsEqual(prop.GetValue(model), newValue))
            {
                return false;
            }

            prop.SetValue(model, newValue, null);

            if (!TryRaisePropertyChanged(this, propertyName))
            {
                return false;
            }

            return true;
        }
        #endregion

        #region Performance Test Methods
        public void Method1<T>(ref T field, T newValue)
        {
            field = newValue;
        }
        
        public void Method2<T>(Action<T> property, T newValue)
        {
            property(newValue);
        }
        
        public void Method3<T1, T2>(T1 model, T2 newValue, Expression<Func<T1, T2>> propertyExpression)
        {
            MemberExpression expr = (MemberExpression)propertyExpression.Body;
            PropertyInfo prop = (PropertyInfo)expr.Member;
            prop.SetValue(model, newValue, null);
        }
        
        public void Method4<T1, T2>(T2 model, T1 newValue, string targetPropertyName)
        {
            PropertyInfo prop = model.GetType().GetProperty(targetPropertyName);
            prop.SetValue(model, newValue, null);
        }
        #endregion
    }
}
